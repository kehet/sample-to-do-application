Sample to-do application using Slim

![Screenshot](screenshot.png)

installing dependencies
=======================
```
composer install
```

```
bower install
```

database schema
===============
```sql
CREATE TABLE `items` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`text` TEXT NOT NULL,
	`done` TINYINT(1) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB;
```