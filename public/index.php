<?php

require '../vendor/autoload.php';

// connect to db
$dbh = new PDO('mysql:dbname=todo;host=localhost', 'php', 'php');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// init slim
$app = new \Slim\Slim(
    [
        'debug' => false,
        'templates.path' => '../partials/',
        'baseUrl' => 'http://localhost/todo/public',
    ]
);

// handle frontpage
$app->get(
    '/',
    function () use ($app, $dbh) {
        $items = $dbh->query('SELECT * FROM `items`');

        $itemsBuffer = [];
        while (($item = $items->fetchObject()) !== false) {
            $itemsBuffer[] = (object)[
                'id' => $item->id,
                'text' => $item->text,
                'done' => ($item->done == '1' ? true : false),
            ];
        }

        $app->render(
            'index.php',
            [
                'items' => $itemsBuffer,
                'baseUrl' => $app->config('baseUrl')
            ]
        );
    }
);

// list items
$app->get(
    '/api/items',
    function () use ($app, $dbh) {
        try {
            $items = $dbh->query('SELECT * FROM `items`');

            $outputBuffer = [];
            while (($item = $items->fetchObject()) !== false) {
                $outputBuffer[] = [
                    'id' => $item->id,
                    'text' => $item->text,
                    'done' => ($item->done == '1' ? true : false),
                ];
            }

            echo json_encode(
                [
                    'status' => 'ok',
                    'items' => $outputBuffer,
                ]
            );
        } catch (PDOException $e) {
            $app->response()->status(500);
            if ($app->config('debug')) {
                echo json_encode(
                    [
                        'status' => 'nok',
                        'error' => $e->getMessage(),
                        'trace' => $e->getTraceAsString(),
                    ]
                );
            } else {
                echo json_encode(
                    [
                        'status' => 'nok',
                    ]
                );
            }
        }
    }
);

// add new item
$app->put(
    '/api/items/add',
    function () use ($app, $dbh) {
        try {
            $query = $dbh->prepare("INSERT INTO `items` (`text`) VALUES (:text);");
            $query->bindValue(':text', $app->request->put('text'));
            $query->execute();

            echo json_encode(
                [
                    'status' => 'ok',
                    'id' => $dbh->lastInsertId(),
                ]
            );
        } catch (PDOException $e) {
            $app->response()->status(500);
            if ($app->config('debug')) {
                echo json_encode(
                    [
                        'status' => 'nok',
                        'error' => $e->getMessage(),
                        'trace' => $e->getTraceAsString(),
                    ]
                );
            } else {
                echo json_encode(
                    [
                        'status' => 'nok',
                    ]
                );
            }
        }
    }
);

// toggle item
$app->post(
    '/api/items/toggle',
    function () use ($app, $dbh) {
        try {
            $sql = <<<SQL
SELECT *
FROM `items`
WHERE `id` = :id
SQL;
            $query = $dbh->prepare($sql);
            $query->bindValue(':id', $app->request->post('id'), PDO::PARAM_INT);
            $query->execute();

            $object = $query->fetchObject();

            if ($object == false) {
                throw new Exception("Item with id '" . $app->request->post('id') . "' not found");
            }

            $isDone = ($object->done === "1");

            $sql = <<<SQL
UPDATE `items`
SET `done` = :isDone
WHERE `id` = :id
SQL;
            $query = $dbh->prepare($sql);
            $query->bindValue(':isDone', ($isDone ? 0 : 1), PDO::PARAM_INT);
            $query->bindValue(':id', $app->request->post('id'), PDO::PARAM_INT);
            $query->execute();

            echo json_encode(
                [
                    'status' => 'ok',
                ]
            );
        } catch (PDOException $e) {
            $app->response()->status(500);
            if ($app->config('debug')) {
                echo json_encode(
                    [
                        'status' => 'nok',
                        'error' => $e->getMessage(),
                        'trace' => $e->getTraceAsString(),
                    ]
                );
            } else {
                echo json_encode(
                    [
                        'status' => 'nok',
                    ]
                );
            }
        } catch (Exception $e) {
            $app->response()->status(500);
            if ($app->config('debug')) {
                echo json_encode(
                    [
                        'status' => 'nok',
                        'error' => $e->getMessage(),
                    ]
                );
            } else {
                echo json_encode(
                    [
                        'status' => 'nok',
                    ]
                );
            }
        }
    }
);

// delete item
$app->delete(
    '/api/items/delete/',
    function () use ($app, $dbh) {
        try {

            $sql = <<<SQL
DELETE FROM `items`
WHERE `id` = :id
SQL;
            $query = $dbh->prepare($sql);
            $query->bindValue(':id', $app->request->post('id'), PDO::PARAM_INT);
            $query->execute();

            echo json_encode(
                [
                    'status' => 'ok',
                ]
            );
        } catch (PDOException $e) {
            echo json_encode(
                [
                    'status' => 'nok',
                    'error' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                ]
            );
        }
    }
);

$app->run();
