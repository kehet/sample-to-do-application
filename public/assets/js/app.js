'use strict';

$(function () {
    // for responsive headline
    jQuery("#headline").fitText();

    // cache
    var todoForm = $('#todo-form');
    var todoText = $("#todo-text");
    var todoItemsInProgress = $('ul#todo-items-in-progress');
    var todoItemsDone = $('ul#todo-items-done');

    // init
    $('li').each(function() {
        registerChange($(this));
    });

    // react to adding items
    todoForm.submit(function (event) {
        event.preventDefault();

        if (todoText.val() != "") {

            // set input box readonly while waiting for request
            todoText.attr('readonly', 'readonly');

            addItem(
                todoText.val(),
                function (data) {       // done
                    addHtml(todoText.val(), data.id);
                    todoText.val("");
                },
                function (jqXHR) {      // fail
                    console.log("error: %o", jqXHR);
                },
                function() {            // always
                    todoText.removeAttr('readonly');
                }
            );
        }
    });

    function addHtml(text, id, isDone) {
        // decide which list item is going
        var list = (isDone ? todoItemsDone : todoItemsInProgress);

        // make and insert item
        list.append(
            "<li class=\"list-group-item" + (isDone ? " disabled" : "") + "\" data-id=\"" + id + "\">" +
                "<div class=\"checkbox\">" +
                    "<input id=\"checkbox" + id + "\" type=\"checkbox\" " + (isDone ? "checked=\"checked\"" : "") + "> " +
                    "<label for=\"checkbox" + id + "\">" +
                        text +
                    "</label>" +
                    "<div class=\"pull-right\">" +
                        "<a href=\"#\"><i class=\"fa fa-trash-o\"></i></a>" +
                    "</div>" +
                "</div>" +
            "</li>"
        );

        // register for events
        registerChange($("li[data-id=" + id + "]"));
    }

    function registerChange(that) {
        var li = that;

        // checkbox
        $('input', li).click(function() {
            var input = $(this);
            var checked = input.prop("checked");

            console.log("%i changed to %o", li.data('id'), checked);

            // move item to another list
            if(checked) {
                li.addClass("disabled");
                li.detach().appendTo(todoItemsDone);
            } else {
                li.removeClass("disabled");
                li.detach().appendTo(todoItemsInProgress);
            }

            // add spinner
            $(".pull-right", li).html("<i class=\"fa fa-refresh fa-spin\"></i>");

            toggleItem(
                li.data('id'),
                function() {            // done
                    // remove spinner
                    $(".pull-right", li).html("<a href=\"#\"><i class=\"fa fa-trash-o\"></i></a>");
                },
                function(jqXHR) {       // fail
                    // replace spinner with error icon
                    $('i', li).removeClass('fa-refresh fa-spin').addClass('fa-ban text-danger');

                    // return to previous state
                    input.prop("checked", "checked");

                    if(!checked) {
                        li.addClass("disabled");
                        li.detach().appendTo(todoItemsDone);
                    } else {
                        li.removeClass("disabled");
                        li.detach().appendTo(todoItemsInProgress);
                    }

                    console.log("error: %o", jqXHR);
                }
            );
        });

        // link for deleting
        $("a", li).click(function (event) {
            event.preventDefault();

            console.log("trying to delete %i", li.data('id'));

            // add spinner
            $(".pull-right", li).html("<i class=\"fa fa-refresh fa-spin\"></i>");

            deleteItem(
                li.data('id'),
                function() {            // done
                    // remove element from dom
                    li.remove();
                },
                function (jqXHR) {      // fail
                    $('i', li).removeClass('fa-refresh fa-spin').addClass('fa-ban text-danger');
                    console.log("error: %o", jqXHR);
                }
            );
        });
    }

    //
    // functions for communicating with backend
    //

    function getItems(done, fail, always) {
        $.ajax({
            type: "GET",
            url: BASEURL + "/api/items",
            dataType: "json"
        })
            .done(done)      // data, textStatus, jqXHR
            .fail(fail)      // jqXHR, textStatus, errorThrown
            .always(always); // data|jqXHR, textStatus, jqXHR|errorThrown
    }

    function addItem(text, done, fail, always) {
        $.ajax({
            type: "PUT",
            url: BASEURL + "/api/items/add",
            dataType: "json",
            data: {
                "text": text
            }
        })
            .done(done)
            .fail(fail)
            .always(always);
    }

    function toggleItem(id, done, fail, always) {
        $.ajax({
            type: "POST",
            url: BASEURL + "/api/items/toggle",
            dataType: "json",
            data: {
                "id": id
            }
        })
            .done(done)
            .fail(fail)
            .always(always);
    }

    function deleteItem(id, done, fail, always) {
        $.ajax({
            type: "DELETE",
            url: BASEURL + "/api/items/delete",
            dataType: "json",
            data: {
                "id": id
            }
        })
            .done(done)
            .fail(fail)
            .always(always);
    }
});