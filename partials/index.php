<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>todo sample app</title>
    <link rel="stylesheet" href="assets/css/styles.css">
</head>
<body>

<div class="container">
    <h1 id="headline">todo sample app</h1>

    <form method="POST" action="?" id="todo-form">
        <div class="row">
            <div class="col-lg-push-2 col-lg-8">
                <input type="hidden" name="_METHOD" value="PUT">
                <input type="text" name="todo" placeholder="What needs to be done?" id="todo-text" class="form-control input-lg">
            </div>
        </div>
    </form>

    <ul id="todo-items-in-progress" class="list-group">
        <?php foreach($items as $item) : ?>
            <?php if($item->done) { continue; } ?>
            <li class="list-group-item" data-id="<?= $item->id ?>">
                <div class="checkbox">
                    <input id="checkbox<?= $item->id ?>" type="checkbox">
                    <label for="checkbox<?= $item->id ?>">
                        <?= $item->text ?>
                    </label>
                    <div class="pull-right"><a href="#"><i class="fa fa-trash-o"></i></a></div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>

    <ul id="todo-items-done" class="list-group">
        <?php foreach($items as $item) : ?>
            <?php if(!$item->done) { continue; } ?>
            <li class="list-group-item disabled" data-id="<?= $item->id ?>">
                <div class="checkbox">
                    <input id="checkbox<?= $item->id ?>" type="checkbox" checked="checked">
                    <label for="checkbox<?= $item->id ?>">
                        <?= $item->text ?>
                    </label>
                    <div class="pull-right"><a href="#"><i class="fa fa-trash-o"></i></a></div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

<footer class="footer">
    <div class="container text-center">
        <small class="copyright">
            Made with <i class="fa fa-heart"></i> by Kehet (with <a href="humans.txt">some help</a>)
        </small>
    </div>
</footer>

<script>var BASEURL = "<?= $baseUrl ?>";</script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/FitText.js/jquery.fittext.js"></script>
<script src="assets/js/app.min.js"></script>

</body>
</html>